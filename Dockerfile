FROM java:8
EXPOSE 8080
ADD /target/DockerHello-0.0.1-SNAPSHOT.jar DockerHello-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","DockerHello-0.0.1-SNAPSHOT.jar"]