HelloDocker -
=================
Using Dockerfile in maven project.

## Development setup
-- Dependencies
* Java 8 or newer

## How to configure
Edit `Dockerfile` if you want to change the ports, etc.

## How to build
Run `mvn package` to create a JAR file in the `target` folder.

Navigate to the project folder and type following command you will be able to create image:

$ docker build -f Dockerfile -t hellodockerimage .

## How to release/deploy
Run 
$ docker run -t hellodockerimage

## Release History
0.0.1
CHANGE: New Develop


###################################################

- Open up your terminal and navigate to your project directory.
- git init
- git remote add origin https://gitlab.com/mydocker/HelloDocker.git
- git add .
- git commit -m "Initial commit"
- git push -u origin master

$ git rm target
$ git commit --amend


